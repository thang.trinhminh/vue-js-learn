const events = new Map();

export default {
    $on(eventName, fn) {
        if (!events.has(eventName)) {
            events.set(eventName, []);
        }

        events.get(eventName).push(fn);
    },

    $off() {
        throw { message: 'Not impllemented'};
    },

    $emit(eventName, data) {
        if (events.has(eventName)) {
            events.get(eventName).forEach(ele => ele(data));
        }
    }
};